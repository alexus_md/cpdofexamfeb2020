package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void testFindMaxLeft() {
        int result = new MinMax().findMax(3, 1);
        assertEquals(3, result);
    }
    @Test
    public void testFindMaxRight() {
        int result = new MinMax().findMax(1, 3);
        assertEquals(3, result);
    }

    @Test
    public void testBar() {

        String inpstr = "test1";
        String result = new MinMax().bar(inpstr);
        assertTrue(inpstr.equalsIgnoreCase(result));
    }

    @Test
    public void testBarNull() {
        String result = new MinMax().bar(null);
        assertNull( result);
    }

    @Test
    public void testBarEmptyStr() {
        String result = new MinMax().bar("");
        assertTrue( result.isEmpty());
    }

}
