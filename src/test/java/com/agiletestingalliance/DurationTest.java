package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class DurationTest {

  @Test
  public void testDur() {
    String result = new Duration().dur();
    assertTrue(result.contains("CP-DOF is designed specifically for corporates and "));
  }

  @Test
  public void testCalculateIntValue() {

    int result = new Duration().calculateIntValue();
    assertEquals(6, result);
  }
}
